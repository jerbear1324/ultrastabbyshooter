﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

	//Pawns have the function but it is expected to overwrite it.
	public virtual void Move (Vector3 direction) {
	}

}
