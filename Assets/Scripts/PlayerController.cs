﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {
	public float speed = 0.0f;
	public float rotationSpeed = 0.0f;
	//Speed / Rotation
	public float reverseSpeed = 0.0f;
	private float normSpeed = 0.0f;
	//Reverse Speed / Normal Speed
	public float fast = 0.0f;
	public float normal = 0.0f;
	private float slower = .01f;
	//Modifiers to speed and variables
	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform>();
		//Removed some items from previous iteration of movement script.
		//Changed information to streamline the design of the level
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (PlayerOneUp)) {
			tf.position += tf.right * speed * normSpeed * slower;
		}
		//Move Forward
		//fixed rate of speed
			if (Input.GetKey (PlayerOneLeft)) {
			tf.Rotate (Vector3.forward, Time.deltaTime * rotationSpeed);
		}
		//turn left
			if (Input.GetKey (PlayerOneBack)) {
			tf.position -= tf.right * speed * slower;
		}
		//Move backwards - Slower than forward
			if (Input.GetKey (PlayerOneRight)) {
			tf.Rotate (-Vector3.forward, Time.deltaTime * rotationSpeed);
		}
		//turn right
		if (Input.GetKey (KeyCode.LeftShift)) {
			normSpeed = fast;
		} else {
			normSpeed = normal;
			//Set for the OverDrive mechanic
		}
	}
}
