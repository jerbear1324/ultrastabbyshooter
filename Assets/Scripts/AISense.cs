﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISense : MonoBehaviour {
	public float hearingModifier;
	//0 = Deaf, 1 = Normal, 2 = Sensitive Hearing

	public float fieldOfView;
	public float viewDistance;

	//Call function when AI tries to listen
	public bool CanHear (GameObject target) {
		//Can it make noise?
		NoiseMaker noiseMaker = target.GetComponent<NoiseMaker> ();
		if (noiseMaker == null) {
			return false;
		}
		// Is it making noise?
		if (noiseMaker.volume <= 0) {
			return false;
		}

		//Is the noise loud enough?
		//Am I close enough to hear it?
		//Get Distance
		float distance = Vector3.Distance (GetComponent<Transform> ().position,
			                 target.GetComponent<Transform> ().position);

		//Caculate the modified required distance
		float modDistance = noiseMaker.volume * hearingModifier;

		//check if close enough
		if (distance <= modDistance) {
			return true;
		}


		//otherwise
		return false;
	}



	public bool CanSee ( GameObject target ) {
		//field of view
		Vector3 forwardVector = GetComponent<Transform> ().forward;
		Vector3 vectorToTarget = target.GetComponent<Transform> ().position - GetComponent<Transform> ().position;

		float angleToTarget = Vector3.Angle (forwardVector, vectorToTarget);
		if (angleToTarget <= fieldOfView) {
			return true;
		}
		//line of sight

		//Otherwise
		return false;
	}
}
