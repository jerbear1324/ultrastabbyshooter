﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPawn : Pawn {
	public enum AIStates {Start, TurnRed, TurnGreen};
	public AIStates currentState;
	public Transform target;
	public Transform myself;
	public float movementSpeed;


	void Update () {
		switch (currentState) {
		case AIStates.Start:
			//Idle
			DoStart ();
			if (Vector3.Distance (target.position, myself.position) < 5) {
				currentState = AIStates.TurnRed;
				Debug.Log ("Red");
			}
			if (Vector3.Distance (target.position, myself.position) > 10) {
				currentState = AIStates.TurnGreen;
				Debug.Log ("Green");
			}
			break;
		}
	}
	void DoStart () {
		//Not a damn thing
	}

	void DoTurnRed () {
		transform.LookAt (target);
		myself.position += myself.right * movementSpeed;
	}

	void DoTurnGreen () {
		Debug.Log ("GREEN!");
	}
}