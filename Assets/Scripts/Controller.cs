﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
	public Pawn pawn;
	public KeyCode PlayerOneUp = KeyCode.W;
	public KeyCode PlayerOneLeft = KeyCode.A;
	public KeyCode PlayerOneRight = KeyCode.D;
	public KeyCode PlayerOneBack = KeyCode.S;
}