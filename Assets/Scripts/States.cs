﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class States : MonoBehaviour {
	public Transform otherObject;
	public enum AIStates {Start, TurnRed, TurnGreen};
	public AIStates currentState;
	private Transform tf;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		switch (currentState) {
		case AIStates.Start:
			DoStart ();
			if (Vector3.Distance (tf.position, otherObject.position) < 5) {
				currentState = AIStates.TurnRed;
			}
			if (Vector3.Distance (tf.position, otherObject.position) > 10) {
				currentState = AIStates.TurnGreen;
			}
			break;
		}
	}
	void DoStart () {
		//Not a damn thing
	}

	void DoTurnRed () {
		SpriteRenderer sr;
		sr = GetComponent<SpriteRenderer> ();
		sr.color = Color.red;
	}

	void DoTurnGreen () {
		SpriteRenderer sr;
		sr = GetComponent<SpriteRenderer> ();
		sr.color = Color.red;
	}
}
