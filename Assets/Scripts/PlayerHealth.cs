﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {
	public float playerHealth;
	public float startingHealth;
	public Transform Player;

	// Use this for initialization
	void Start () {
		playerHealth = startingHealth;
		
	}
	
	// Update is called once per frame
	void Update (float damage) {
		if (playerHealth == 0) {
			Destroy(Player);
		}
	}
	public void DamagePlayer (float damage)
	{
		playerHealth = playerHealth - damage;
	}
}
