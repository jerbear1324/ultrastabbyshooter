﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsScene : MonoBehaviour {
	public int loadTime;

	// Use this for initialization
	void Start () {
		StartCoroutine (LoadDelay ());
		{
			SceneManager.LoadScene("Options");
		}
	}
	IEnumerator LoadDelay() {
		yield return new WaitForSeconds (loadTime);
	}
}