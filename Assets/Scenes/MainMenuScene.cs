﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScene : MonoBehaviour {
	public int loadTime;

	// Use this for initialization
	void Start () {
		StartCoroutine (LoadDelay ());
		{
			SceneManager.LoadScene("MainMenu");
		}
	}
	IEnumerator LoadDelay() {
		yield return new WaitForSeconds (loadTime);
	}
}